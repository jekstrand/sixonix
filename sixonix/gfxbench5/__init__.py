from .run import run

BENCHMARKS = [
    'aztec_ruins_gl_high',
    'aztec_ruins_gl_normal',
    'aztec_ruins_gl_high_o',
    'aztec_ruins_gl_normal_o',
    'aztec_ruins_vk_high',
    'aztec_ruins_vk_normal',
    'aztec_ruins_vk_high_o',
    'aztec_ruins_vk_normal_o',
]
